# ATLAS Ntuple-maker for MC16 xAOD Samples

This repository contains codes to convert ATLAS MC16 xAOD samples to easy-to-use ntuples easily accessible by ROOT. The codes are based on AnalysisBase and EventLoop instead of Athena.

This repository is developed and maintained by the SLAC ATLAS Group.

**Current Contributors**
* Sanha Cheong
* Aviv Cukierman
* Murtaza Safdari

## Software Versions

This repository is developed and must be used with:
* ATLAS MC16 xAOD samples (might be applicable to MC15 samples as well, but this is not guranteed)
* ATLAS Analysis Release: `AnalysisBase,21.2.72`
* Python 2.7
* `cmake 3.11.0`

The appropriate Python and `cmake` versions should come along with the analysis release setup:
```
setupATLAS
cd ./build/
asetup AnalysisBase,21.2.72
```
If this is not your first-time running the code, you can also do `asetup --restore` instead in the build directory; this will set-up the same release you previously used.

## Directory Structures and How-to's

### `source` directory

This is where all the source codes are.
* The main ntuple-maker: `source/NtupleMaker/Root/NtupleMakerAna.cxx`
* The header: `source/NtupleMaker/NtupleMaker/NtupleMakerAna.h`
* Library, etc. list for `cmake`: `source/NtupleMaker/CMakeLists.txt`

`source` directory also contains a Python macro to actually run the ntuple-maker: `ATestRun.py`. See the `run` directory section below for more details.


### `build` directory

This is where we compile the package with `cmake` and `make`.

First, in every new terminal session before compiling anything, run:
```
cd ./build/
source setup_build.sh
```
This prepares the `build` directory for compiling.

After that, you can run `cmake ../source/` to configure the Analysis Release and the build of this package, `NtupleMaker`. To actually compile the C++ codes, run `make` inside the build directory, after you have run `cmake ../source/`.

(You only need to run `cmake ../source/` when you have added (removed) a new file to (from) the packages. When you have only edited existing files, you can simply recompile with `make`.)

### `run` directory

This is where you actually run the ntuple-maker and/or other analyses algorithms. After having compiled the package inside the `build` directory, you can run the ntuple-maker by:
```
cd ./run/
../source/ATestRun.py
```
This will run the ntuple-maker macro `./source/ATestRun.py` which includes the run configurations, the xAOD file(s) to process and analyze, etc.

The outputs are stored in the directory `./run/submitDir` by default. In particular, the processed ntuples are saved in `./run/submitDir/data-ANALYSIS/` as `.root` files.

*NOTE*: The script will fail if the directory `submitDir` already exists in the `run` directory. The submission directory can be specified as an option in the run script. Also, if run with `-w`, the run script automatically overwrites the submission directory. 
