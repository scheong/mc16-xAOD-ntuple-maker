#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option("-s", "--submitDir", dest = 'submitDir', action = 'store', type = 'string', default = 'submitDir', help = 'Submission directory for EventLoop' )
parser.add_option("-w", "--overwrite", action='store_true', default=False, help="overwrite previous submitDir")
parser.add_option("-v", "--verbose", action='store_true', default=False, help="verbose output")
parser.add_option("--inputFileList", dest='use_inputFileList', action='store_true', help="explicitly list input files in text file (specify text file with --inputFiles)", default=False )
parser.add_option("--inputFiles", type=str, dest='search_directories', help="Search directory for samples. If dir1/dir2/file.AOD.root, then should be \"dir1\". If --inputFileList, then should just be \"filelist.txt\".", default = "/atlas/local/sanha/mc16_dijet/")
parser.add_option("--driver", help="select where to run", choices=("direct", "lsf"), default="direct")
parser.add_option("--filesPerJob", help="how many files to submit per job", type=int, default=1)
parser.add_option("--numEvents", type=int, help="number of events to process for all the datasets")
parser.add_option("--skipEvents", type=int, help="skip the first n events")
parser.add_option("--doSampleWeights", action='store_true', help="Use Sample Weights", default=False )
parser.add_option("--sampleWeights", type=str, help="File containing sample weights", default = "")
parser.add_option("--sampleName", type=str, help="String indicating sample (used be sample weights file)", default = "")
parser.add_option("--sampleDir", type=str, dest='direct_directory', help="Directory for samples. If dir1/dir2/file.AOD.root, then should be \"dir1/dir2/\"", default = "")

#Analysis configs
parser.add_option("--doEventShape", action='store_true', help="Store event shape info", default=False)
parser.add_option("--doLargeRJets", action='store_true', help="Store large R jets info", default=False)
parser.add_option("--doTruthLargeRJets", action='store_true', help="Store large R jets truth info", default=False)
parser.add_option("--doJets", action='store_true', help="Store small R jets info", default=False)
parser.add_option("--doTruthJets", action='store_true', help="Store small R jets truth info", default=False)
parser.add_option("--doVertices", action='store_true', help="Store vertex info", default=False)
parser.add_option("--doTruthVertices", action='store_true', help="Store vertex truth info", default=False)
parser.add_option("--doPhotons", action='store_true', help="Store photon info", default=False)
parser.add_option("--doTruthPhotons", action='store_true', help="Store photon truth info", default=False)
parser.add_option("--doTopoClusters", action='store_true', help="Store topocluster info", default=False)
parser.add_option("--doTracks", action='store_true', help="Store jet and vtx track info", default=False)
parser.add_option("--hasTruthPileupContainer", action='store_true', help="Data has TruthPileupEvents Container", default=False)
parser.add_option("--isHLMC15", action='store_true', help="Is data HLLHC MC15 ?", default=False)
parser.add_option("--isMC16", action='store_true', help="Is data MC16 ?", default=False)
( options, args ) = parser.parse_args()

import os
# check submission directory
if os.path.exists(options.submitDir):
  if options.overwrite:
    print "removing {0:s}".format(options.submitDir)
    import shutil
    shutil.rmtree(options.submitDir, True)
  else:
    raise OSError('Output directory {0:s} already exists. Either re-run with -w (overwrite), choose a different --submitDir, or rm -rf it yourself.'.format(options.submitDir))

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

if options.driver == 'lsf':
  if getattr(ROOT.EL, 'LSFDriver') is None:
    raise KeyError('Cannot load the LSF driver from EventLoop. Did you not compile it?')

sh = ROOT.SH.SampleHandler()

# scan for datasets in the given directories
directory = options.search_directories
if options.use_inputFileList:
  ROOT.SH.readFileList(sh, "sample", directory)
else:
#   if options.use_inputSampleDirectory:
#     ROOT.SH.ScanDir().filePattern( "*" ).scan( sh, options.direct_directory )
  if (options.direct_directory is not ""):
    ROOT.SH.ScanDir().filePattern( "*" ).scan( sh, options.direct_directory )
  else:
    ROOT.SH.scanDir(sh, directory)

#inputFilePath = "/atlas/local/sanha/mc16_dijet/mc16_13TeV.426001.PowhegPythia8EvtGen_A14_NNPDF23LO_CT10ME_jetjet_JZ1.recon.AOD.e3788_s3126_r10201"
#ROOT.SH.ScanDir().filePattern( "AOD.13306030._000017.pool.root.1" ).scan( sh, inputFilePath )

sh.setMetaString( 'nc_tree', 'CollectionTree' )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )

if options.driver == 'lsf':
  if 'WorkDir_DIR' not in os.environ: os.environ['WorkDir_DIR']=os.environ['UserAnalysis_DIR'] #otherwise error
  job.options().setBool(ROOT.EL.Job.optResetShell, False);
  job.options().setDouble(ROOT.EL.Job.optFilesPerWorker, options.filesPerJob)

if options.numEvents:
  job.options().setDouble(ROOT.EL.Job.optMaxEvents, options.numEvents)

if options.skipEvents:
  job.options().setDouble(ROOT.EL.Job.optSkipEvents, options.skipEvents)

#Set the xAOD access mode of the job:
#job.options().setString( ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_branch );
#job.options().setString( ROOT.EL.Job.optXaodAccessMode,ROOT.EL.Job.optXaodAccessMode_athena );

overallWeight = 1.
if (options.doSampleWeights):
  if os.path.exists(options.sampleWeights):
    import json
    try:
      with open(options.sampleWeights,'rb') as f:
        data = json.load(f)
        weightsData = data[options.sampleName]
        overallWeight = weightsData['xsec']*weightsData['filtereff']/weightsData['sumWeights'] #event weight = overallWeight*mcEventWeight*lumi
    except: print 'Failed to read sample weights json - exists, but failed'
  else: print 'Failed to read sample weights json - does not exist'

# Configure analysis output
job.outputAdd (ROOT.EL.OutputStream ("ANALYSIS"))

# Create the algorithm's configuration. Note that we'll be able to add
# algorithm property settings here later on.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
config = AnaAlgorithmConfig( 'NtupleMakerAna/AnalysisAlg' )

config.Verbose           = options.verbose
config.doEventShape      = options.doEventShape
config.doLargeRJets      = options.doLargeRJets
config.doTruthLargeRJets = options.doTruthLargeRJets
config.doJets            = options.doJets
config.doTruthJets       = options.doTruthJets
config.doVertices        = options.doVertices
config.doTruthVertices   = options.doTruthVertices
config.doPhotons         = options.doPhotons
config.doTruthPhotons    = options.doTruthPhotons
config.doTopoClusters    = options.doTopoClusters
config.doTracks          = options.doTracks
config.isMC16            = options.isMC16
config.isHLMC15          = options.isHLMC15
config.overallWeight     = overallWeight
config.hasTruthPileupContainer = options.hasTruthPileupContainer

job.algsAdd( config )

driver = None
if (options.driver == "direct"):
  #Run the job using the direct driver.
  driver = ROOT.EL.DirectDriver()
  driver.submit( job, options.submitDir )
elif (options.driver == "lsf"):
  #Run the job using LSF.
  driver = ROOT.EL.LSFDriver()
  #driver.options().setString(ROOT.EL.Job.optSubmitFlags, '-q medium -W200')
  driver.options().setString(ROOT.EL.Job.optSubmitFlags, '-q atlas-t3 -W300')
  driver.submitOnly(job, options.submitDir)
