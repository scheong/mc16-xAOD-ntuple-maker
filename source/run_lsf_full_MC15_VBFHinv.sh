../source/runScript.py \
  -w \
  -s /nfs/slac/g/atlas/u02/murtazas/mc16-xAOD-ntuple-maker/run/submitDir_lsf_HLLHCMC15_10899 \
  --sampleDir "/nfs/slac/g/atlas/u02/sanha/data_storage/VBF_H_inv/mc15_14TeV.301399.PowhegPythia8EvtGen_CT10_AZNLOCTEQ6L1_VBFH125_ZZ4nu.recon.AOD.e4956_s3348_s3347_r10899/" \
  --isHLMC15 --hasTruthPileupContainer\
  --doVertices --doTruthVertices --doTracks \
  --doJets --doTruthJets \
  --driver lsf \
  --filesPerJob 1
