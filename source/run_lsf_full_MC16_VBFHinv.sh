../source/runScript.py \
  -w \
  -s /nfs/slac/g/atlas/u02/murtazas/mc16-xAOD-ntuple-maker/run/submitDir_lsf_MC16e \
  --sampleDir "/nfs/slac/g/atlas/u02/murtazas/data_store/VBFHinv/mc16e/mc16_13TeV.308567.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_ZZ4nu_125MET.merge.AOD.e6126_e5984_s3126_r10724_r10726_tid16550820_00/" \
  --isMC16 --hasTruthPileupContainer\
  --doVertices --doTruthVertices --doTracks \
  --doJets --doTruthJets\
  --driver lsf \
  --filesPerJob 1
