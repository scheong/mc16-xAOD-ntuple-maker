// C++ stuff
#include <vector>

// Root stuff
#include <TTree.h>

// Basic Analysis set-ups
#include <AsgTools/MessageCheck.h>
#include "xAODBase/IParticleHelpers.h"
#include <NtupleMaker/NtupleMakerAna.h>

// xAOD: Event-level info. and meta data
#include <xAODEventInfo/EventInfo.h>
#include <xAODEventShape/EventShape.h>
// xAOD: Truth-level info.
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthPileupEventContainer.h"
// xAOD: Jet stuff
#include <xAODJet/JetContainer.h>
// xAOD: Calorimeter Topo-cluster
#include <xAODCaloEvent/CaloClusterContainer.h>
// xAOD: Photon stuff
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/Photon.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#define ARRAY_INIT {}

NtupleMakerAna :: NtupleMakerAna (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
      m_jetCleaning ("JetCleaningTool/JetCleaning", this),
      m_JERTool ("JERTool", this),
      m_jetCalibTool("JetCalibrationTool", this),
      m_largeRJetCalibTool("JetCalibrationTool/LargeR", this),
      m_jetTrkPt(ARRAY_INIT),
      m_jetTrkEta(ARRAY_INIT),
      m_jetTrkPhi(ARRAY_INIT),
      m_jetTrkZ0(ARRAY_INIT),
      m_jetTrkD0(ARRAY_INIT),
      m_jetTrkZ0err(ARRAY_INIT),
      m_jetTrkD0err(ARRAY_INIT),
      // m_jetTrkW(ARRAY_INIT),
      m_jetTrkVertex(ARRAY_INIT),
      m_jetTrkTruthVertex(ARRAY_INIT),
      // m_jetTruthFraction(ARRAY_INIT),
      m_vtxTrkPt(ARRAY_INIT),
      m_vtxTrkEta(ARRAY_INIT),
      m_vtxTrkPhi(ARRAY_INIT),
      m_vtxTrkZ0(ARRAY_INIT),
      m_vtxTrkD0(ARRAY_INIT),
      m_vtxTrkZ0err(ARRAY_INIT),
      m_vtxTrkD0err(ARRAY_INIT),
      m_vtxTrkW(ARRAY_INIT),
      m_vtxTrkVertex(ARRAY_INIT),
      m_vtxTrkTruthVertex(ARRAY_INIT),
      m_jetE_constit (ARRAY_INIT),
      m_jetPt_constit (ARRAY_INIT),
      m_jetEta_constit (ARRAY_INIT),
      m_jetPhi_constit (ARRAY_INIT),
      m_jetM_constit (ARRAY_INIT),
      m_jetE_calib (ARRAY_INIT),
      m_jetPt_calib (ARRAY_INIT),
      m_jetEta_calib (ARRAY_INIT),
      m_jetPhi_calib (ARRAY_INIT),
      m_jetM_calib (ARRAY_INIT),
      m_jetTiming (ARRAY_INIT),
      m_jetNegE (ARRAY_INIT),
      m_jetCharge (ARRAY_INIT),
      m_jetAngularity (ARRAY_INIT),
      m_jetFlavor (ARRAY_INIT),
      m_largeRjetE_constit (ARRAY_INIT),
      m_largeRjetPt_constit (ARRAY_INIT),
      m_largeRjetEta_constit (ARRAY_INIT),
      m_largeRjetPhi_constit (ARRAY_INIT),
      m_largeRjetM_constit (ARRAY_INIT),
      m_largeRjetE_calib (ARRAY_INIT),
      m_largeRjetPt_calib (ARRAY_INIT),
      m_largeRjetEta_calib (ARRAY_INIT),
      m_largeRjetPhi_calib (ARRAY_INIT),
      m_largeRjetM_calib (ARRAY_INIT),
      m_largeRjetFlavor (ARRAY_INIT),
      m_jetType(ARRAY_INIT),
      m_jetTruthPt(ARRAY_INIT),
      m_jetTruthEta(ARRAY_INIT),
      m_jetTruthPhi(ARRAY_INIT),
      m_recovtx_z(ARRAY_INIT),
      m_recovtx_z_err(ARRAY_INIT),
      m_recovtx_sumPt(ARRAY_INIT),
      m_recovtx_WsumPt(ARRAY_INIT),
      m_recovtx_index(ARRAY_INIT),
      m_truthvtx_index(ARRAY_INIT),
      m_truthvtx_z(ARRAY_INIT),
      m_truthvtx_x(ARRAY_INIT),
      m_truthvtx_y(ARRAY_INIT),
      m_truthvtx_t(ARRAY_INIT),
      m_ptnE(ARRAY_INIT),
      m_ptnPt(ARRAY_INIT),
      m_ptnEta(ARRAY_INIT),
      m_ptnPhi(ARRAY_INIT),
      m_ptnIso_topoetcone20(ARRAY_INIT),
      m_ptnIso_ptcone20(ARRAY_INIT),
      // m_ptnVtx_z(ARRAY_INIT),
      m_ptnIsLoose(ARRAY_INIT),
      m_ptnIsTight(ARRAY_INIT),
      m_truthPtnE(ARRAY_INIT),
      m_truthPtnPt(ARRAY_INIT),
      m_truthPtnEta(ARRAY_INIT),
      m_truthPtnPhi(ARRAY_INIT),
      m_clPt(ARRAY_INIT),
      m_clEta(ARRAY_INIT),
      m_clPhi(ARRAY_INIT),
      m_clLambda(ARRAY_INIT),
      m_clLambda2(ARRAY_INIT),
      m_clEmProb(ARRAY_INIT)

{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0. Note that things like resetting
  // statistics variables should rather go into the initialize() function.
  declareProperty("Verbose",           m_verbose = false,           "Verbose output");
  declareProperty("doEventShape",      m_doEventShape = false,      "Do event shape output");
  declareProperty("doLargeRJets",      m_doLargeRJets = false,      "Do large R jets output");
  declareProperty("doTruthLargeRJets", m_doTruthLargeRJets = false, "Do large R jets truth output");
  declareProperty("doJets",            m_doJets = false,              "Do small R jets output");
  declareProperty("doTruthJets",       m_doTruthJets = false,       "Do small R jets truth output");
  declareProperty("doVertices",        m_doVertices = false,        "Do vertex output");
  declareProperty("doTruthVertices",   m_doTruthVertices = false,   "Do vertex truth output");
  declareProperty("doPhotons",         m_doPhotons = false,         "Do photon output");
  declareProperty("doTruthPhotons",    m_doTruthPhotons = false,    "Do photon truth output");
  declareProperty("doTopoClusters",    m_doTopoClusters = false,    "Do topocluster output");
  declareProperty("doTracks",          m_doTracks = false,          "Do jet and vertex track output");
  declareProperty("isHLMC15",          m_isHLMC15 = false,          "Is Sim Data HLLHC MC15 ?");
  declareProperty("isMC16",            m_isMC16 = false,            "Is Sim Data MC16 ?");
  declareProperty("overallWeight",     m_overallWeight = 1.,        "MC norm weight");
  declareProperty("hasTruthPileupContainer", m_hasTruthPileupContainer = false, "Data Has TruthPileupEvent Container");
}



NtupleMakerAna::~NtupleMakerAna (){
  if ( m_jetE_constit ) delete m_jetE_constit; 
  if ( m_jetPt_constit ) delete m_jetPt_constit;
  if ( m_jetEta_constit ) delete m_jetEta_constit;
  if ( m_jetPhi_constit ) delete m_jetPhi_constit;
  if ( m_jetM_constit ) delete m_jetM_constit;
  if ( m_jetE_calib ) delete m_jetE_calib;
  if ( m_jetPt_calib ) delete m_jetPt_calib;
  if ( m_jetEta_calib ) delete m_jetEta_calib;
  if ( m_jetPhi_calib ) delete m_jetPhi_calib;
  if ( m_jetM_calib ) delete m_jetM_calib;
  if ( m_jetTiming ) delete m_jetTiming;
  if ( m_jetNegE ) delete m_jetNegE;
  if ( m_jetCharge ) delete m_jetCharge;
  if ( m_jetAngularity ) delete m_jetAngularity;
  if ( m_jetFlavor ) delete m_jetFlavor;
  if ( m_jetType ) delete m_jetType;
  if ( m_jetTruthPt ) delete m_jetTruthPt;
  if ( m_jetTruthEta ) delete m_jetTruthEta;
  if ( m_jetTruthPhi ) delete m_jetTruthPhi;
  if ( m_jetTrkPt ) delete m_jetTrkPt;
  if ( m_jetTrkEta ) delete m_jetTrkEta;
  if ( m_jetTrkPhi ) delete m_jetTrkPhi;
  if ( m_jetTrkZ0 ) delete m_jetTrkZ0;
  if ( m_jetTrkD0 ) delete m_jetTrkD0;
  if ( m_jetTrkZ0err ) delete m_jetTrkZ0err;
  if ( m_jetTrkD0err ) delete m_jetTrkD0err;
  // if ( m_jetTrkW ) delete m_jetTrkW;
  if ( m_jetTrkVertex ) delete m_jetTrkVertex;
  if ( m_jetTrkTruthVertex ) delete m_jetTrkTruthVertex;
  // if ( m_jetTruthFraction ) delete m_jetTruthFraction;
  if ( m_largeRjetE_constit ) delete m_largeRjetE_constit;
  if ( m_largeRjetPt_constit ) delete m_largeRjetPt_constit; 
  if ( m_largeRjetEta_constit ) delete m_largeRjetEta_constit;
  if ( m_largeRjetPhi_constit ) delete m_largeRjetPhi_constit;
  if ( m_largeRjetM_constit ) delete m_largeRjetM_constit;
  if ( m_largeRjetE_calib ) delete m_largeRjetE_calib;
  if ( m_largeRjetPt_calib ) delete m_largeRjetPt_calib;
  if ( m_largeRjetEta_calib ) delete m_largeRjetEta_calib;
  if ( m_largeRjetPhi_calib ) delete m_largeRjetPhi_calib;
  if ( m_largeRjetM_calib ) delete m_largeRjetM_calib;
  if ( m_largeRjetFlavor ) delete m_largeRjetFlavor; 
  if ( m_recovtx_z ) delete m_recovtx_z;
  if ( m_recovtx_z_err ) delete m_recovtx_z_err;
  if ( m_recovtx_sumPt ) delete m_recovtx_sumPt;
  if ( m_recovtx_WsumPt ) delete m_recovtx_WsumPt;
  if ( m_recovtx_index ) delete m_recovtx_index;
  if ( m_truthvtx_index ) delete m_truthvtx_index;
  if ( m_truthvtx_z ) delete m_truthvtx_z;
  if ( m_truthvtx_x ) delete m_truthvtx_x;
  if ( m_truthvtx_y ) delete m_truthvtx_y;
  if ( m_truthvtx_t ) delete m_truthvtx_t;
  if ( m_vtxTrkPt ) delete m_vtxTrkPt;
  if ( m_vtxTrkEta ) delete m_vtxTrkEta;
  if ( m_vtxTrkPhi ) delete m_vtxTrkPhi;
  if ( m_vtxTrkZ0 ) delete m_vtxTrkZ0;
  if ( m_vtxTrkD0 ) delete m_vtxTrkD0;
  if ( m_vtxTrkZ0err ) delete m_vtxTrkZ0err;
  if ( m_vtxTrkD0err ) delete m_vtxTrkD0err;
  if ( m_vtxTrkW ) delete m_vtxTrkW;
  if ( m_vtxTrkVertex ) delete m_vtxTrkVertex;
  if ( m_vtxTrkTruthVertex ) delete m_vtxTrkTruthVertex;
  if ( m_ptnE ) delete m_ptnE;
  if ( m_ptnPt ) delete m_ptnPt;
  if ( m_ptnEta ) delete m_ptnEta;
  if ( m_ptnPhi ) delete m_ptnPhi;
  if ( m_ptnIso_topoetcone20 ) delete m_ptnIso_topoetcone20;
  if ( m_ptnIso_ptcone20 ) delete m_ptnIso_ptcone20;
  // if ( m_ptnVtx_z ) delete m_ptnVtx_z;
  if ( m_ptnIsLoose ) delete m_ptnIsLoose;
  if ( m_ptnIsTight ) delete m_ptnIsTight;
  if ( m_truthPtnE ) delete m_truthPtnE;
  if ( m_truthPtnPt ) delete m_truthPtnPt;
  if ( m_truthPtnEta ) delete m_truthPtnEta;
  if ( m_truthPtnPhi ) delete m_truthPtnPhi;
  if ( m_clPt ) delete m_clPt;
  if ( m_clEta ) delete m_clEta;
  if ( m_clPhi ) delete m_clPhi;
  if ( m_clLambda ) delete m_clLambda;
  if ( m_clLambda2 ) delete m_clLambda2;
  if ( m_clEmProb ) delete m_clEmProb;
}



StatusCode NtupleMakerAna :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("ntuple", "ntuple tree")));
  TTree* mytree = tree ("ntuple");
  // Branches in ntuple tree
  mytree->Branch ("RunNumber",        &m_runNumber,   "RunNumber/I");
  mytree->Branch ("EventNumber",      &m_eventNumber, "EventNumber/L");
  mytree->Branch ("EventWeight",      &m_eventWeight, "EventWeight/F");
  mytree->Branch ("MU",               &m_MU,          "MU/I");
  // Event Shape
  if(m_doEventShape){
    mytree->Branch ("EventRho",         &m_eventRho,    "EventRho/F");
    mytree->Branch ("EventArea",        &m_eventArea,   "EventArea/F");
    mytree->Branch ("EventSigma",       &m_eventSigma,  "EventSigma/F");
  }
  // Small R Reco. jet
  if(m_doJets){
    // m_jetE =          new std::vector<float>();
    mytree->Branch ("JetE_constit",     "std::vector<float>",      &m_jetE_constit);
    mytree->Branch ("JetPt_constit",    "std::vector<float>",      &m_jetPt_constit);
    mytree->Branch ("JetEta_constit",   "std::vector<float>",      &m_jetEta_constit);
    mytree->Branch ("JetPhi_constit",   "std::vector<float>",      &m_jetPhi_constit);
    mytree->Branch ("JetM_constit",     "std::vector<float>",      &m_jetM_constit);
    mytree->Branch ("JetE_calib",       "std::vector<float>",      &m_jetE_calib);
    mytree->Branch ("JetPt_calib",      "std::vector<float>",      &m_jetPt_calib);
    mytree->Branch ("JetEta_calib",     "std::vector<float>",      &m_jetEta_calib);
    mytree->Branch ("JetPhi_calib",     "std::vector<float>",      &m_jetPhi_calib);
    mytree->Branch ("JetM_calib",       "std::vector<float>",      &m_jetM_calib);
    mytree->Branch ("JetTiming",        "std::vector<float>",      &m_jetTiming);
    mytree->Branch ("JetNegE",          "std::vector<float>",      &m_jetNegE);
    mytree->Branch ("JetCharge",        "std::vector<float>",      &m_jetCharge);
    mytree->Branch ("JetAngularity",    "std::vector<float>",      &m_jetAngularity);
    mytree->Branch ("JetFlavor",        "std::vector<int>",        &m_jetFlavor);
    if(m_doTruthJets){
      mytree->Branch ("JetType",        "std::vector<int>",        &m_jetType);
      mytree->Branch ("JetTruthPt",     "std::vector<float>",      &m_jetTruthPt);
      mytree->Branch ("JetTruthEta",    "std::vector<float>",      &m_jetTruthEta);
      mytree->Branch ("JetTruthPhi",    "std::vector<float>",      &m_jetTruthPhi);
    }
    if(m_doTracks){
      // m_jetTrkPt =          new std::vector<std::vector<float> >();
      mytree->Branch ("JetTrkPt",          "std::vector<std::vector<float> >",      &m_jetTrkPt);
      mytree->Branch ("JetTrkEta",         "std::vector<std::vector<float> >",      &m_jetTrkEta);
      mytree->Branch ("JetTrkPhi",         "std::vector<std::vector<float> >",      &m_jetTrkPhi);
      mytree->Branch ("JetTrkZ0",          "std::vector<std::vector<float> >",      &m_jetTrkZ0);
      mytree->Branch ("JetTrkD0",          "std::vector<std::vector<float> >",      &m_jetTrkD0);
      mytree->Branch ("JetTrkZ0err",       "std::vector<std::vector<float> >",      &m_jetTrkZ0err);
      mytree->Branch ("JetTrkD0err",       "std::vector<std::vector<float> >",      &m_jetTrkD0err);
      // mytree->Branch ("JetTrkW",           "std::vector<std::vector<float> >",      &m_jetTrkW);
      mytree->Branch ("JetTrkVertex",      "std::vector<std::vector<int> >",        &m_jetTrkVertex);
      mytree->Branch ("JetTrkTruthVertex", "std::vector<std::vector<int> >",        &m_jetTrkTruthVertex);
      // mytree->Branch ("JetTruthFraction",  "std::vector<std::vector<float> >",      &m_jetTruthFraction);
    }
  }
  // Large R Reco. jet
  if(m_doLargeRJets){
    mytree->Branch ("LargeRjetE_constit",      "std::vector<float>",      &m_largeRjetE_constit);
    mytree->Branch ("LargeRjetPt_constit",     "std::vector<float>",      &m_largeRjetPt_constit);
    mytree->Branch ("LargeRjetEta_constit",    "std::vector<float>",      &m_largeRjetEta_constit);
    mytree->Branch ("LargeRjetPhi_constit",    "std::vector<float>",      &m_largeRjetPhi_constit);
    mytree->Branch ("LargeRjetM_constit",      "std::vector<float>",      &m_largeRjetM_constit);
    mytree->Branch ("LargeRjetE_calib",        "std::vector<float>",      &m_largeRjetE_calib);
    mytree->Branch ("LargeRjetPt_calib",       "std::vector<float>",      &m_largeRjetPt_calib);
    mytree->Branch ("LargeRjetEta_calib",      "std::vector<float>",      &m_largeRjetEta_calib);
    mytree->Branch ("LargeRjetPhi_calib",      "std::vector<float>",      &m_largeRjetPhi_calib);
    mytree->Branch ("LargeRjetM_calib",        "std::vector<float>",      &m_largeRjetM_calib);
    mytree->Branch ("LargeRjetFlavor",         "std::vector<int>",        &m_largeRjetFlavor);
  }
  if(m_doVertices){
    mytree->Branch ("NPV", &m_NPV, "NPV/I");
    mytree->Branch ("RecoVtx_z",      "std::vector<float>",      &m_recovtx_z);
    mytree->Branch ("RecoVtx_z_err",  "std::vector<float>",      &m_recovtx_z_err);
    mytree->Branch ("RecoVtx_index",  "std::vector<int>",        &m_recovtx_index);
    if(m_doTruthVertices){
      mytree->Branch ("TruthVtx_z",      "std::vector<float>",      &m_truthvtx_z);
      mytree->Branch ("TruthVtx_x",      "std::vector<float>",      &m_truthvtx_x);
      mytree->Branch ("TruthVtx_y",      "std::vector<float>",      &m_truthvtx_y);
      mytree->Branch ("TruthVtx_t",      "std::vector<float>",      &m_truthvtx_t);
      mytree->Branch ("TruthVtx_index",  "std::vector<int>",        &m_truthvtx_index);
    }
    if(m_doTracks){
      mytree->Branch ("RecoVtx_sumPt",  "std::vector<float>",      &m_recovtx_sumPt);
      mytree->Branch ("RecoVtx_WsumPt", "std::vector<float>",      &m_recovtx_WsumPt);
      mytree->Branch ("VtxTrkPt",          "std::vector<std::vector<float> >",      &m_vtxTrkPt);
      mytree->Branch ("VtxTrkEta",         "std::vector<std::vector<float> >",      &m_vtxTrkEta);
      mytree->Branch ("VtxTrkPhi",         "std::vector<std::vector<float> >",      &m_vtxTrkPhi);
      mytree->Branch ("VtxTrkZ0",          "std::vector<std::vector<float> >",      &m_vtxTrkZ0);
      mytree->Branch ("VtxTrkD0",          "std::vector<std::vector<float> >",      &m_vtxTrkD0);
      mytree->Branch ("VtxTrkZ0err",       "std::vector<std::vector<float> >",      &m_vtxTrkZ0err);
      mytree->Branch ("VtxTrkD0err",       "std::vector<std::vector<float> >",      &m_vtxTrkD0err);
      mytree->Branch ("VtxTrkW",           "std::vector<std::vector<float> >",      &m_vtxTrkW);
      mytree->Branch ("VtxTrkVertex",      "std::vector<std::vector<int> >",        &m_vtxTrkVertex);
      mytree->Branch ("VtxTrkTruthVertex", "std::vector<std::vector<int> >",        &m_vtxTrkTruthVertex);
    }
  }
  if(m_doPhotons){
    mytree->Branch ("PhotonE",                 "std::vector<float>",      &m_ptnE);
    mytree->Branch ("PhotonPt",                "std::vector<float>",      &m_ptnPt);
    mytree->Branch ("PhotonEta",               "std::vector<float>",      &m_ptnEta);
    mytree->Branch ("PhotonPhi",               "std::vector<float>",      &m_ptnPhi);
    mytree->Branch ("PhotonIso_topoetcone20",  "std::vector<float>",      &m_ptnIso_topoetcone20);
    mytree->Branch ("PhotonIso_ptcone20",      "std::vector<float>",      &m_ptnIso_ptcone20);
    // mytree->Branch ("PhotonVtx_z="      ,      "std::vector<float>",      &m_ptnVtx_z);
    mytree->Branch ("PhotonIsLoose",           "std::vector<int>",        &m_ptnIsLoose);
    mytree->Branch ("PhotonIsTight",           "std::vector<int>",        &m_ptnIsTight);
    if(m_doTruthPhotons){
      mytree->Branch ("TruthPhotonE",     "std::vector<float>",      &m_truthPtnE);
      mytree->Branch ("TruthPhotonPt",    "std::vector<float>",      &m_truthPtnPt);
      mytree->Branch ("TruthPhotonEta",   "std::vector<float>",      &m_truthPtnEta);
      mytree->Branch ("TruthPhotonPhi",   "std::vector<float>",      &m_truthPtnPhi);
    }
  }
  if(m_doTopoClusters){
    mytree->Branch ("ClstrPt",       "std::vector<float>",      &m_clPt);
    mytree->Branch ("ClstrEta",      "std::vector<float>",      &m_clEta);
    mytree->Branch ("ClstrPhi",      "std::vector<float>",      &m_clPhi);
    mytree->Branch ("ClstrLambda",   "std::vector<float>",      &m_clLambda);
    mytree->Branch ("ClstrLambda2",  "std::vector<float>",      &m_clLambda2);
    mytree->Branch ("ClstrEmProb",   "std::vector<float>",      &m_clEmProb);
  }

  // Initialize and configure the jet cleaning tool
  ANA_CHECK (m_jetCleaning.setProperty( "CutLevel", "LooseBad"));
  ANA_CHECK (m_jetCleaning.setProperty("DoUgly", false));
  ANA_CHECK (m_jetCleaning.initialize());

  // Initialize the JER tool using default configurations
  ANA_CHECK (m_JERTool.initialize());

  // Initialize the JetCalibration tool for AntiK4EMTopoJets
  TString jetAlgo = "AntiKt4EMTopo";
  TString jetCalibArea, jetCalibConfig, jetCalibSeq;
  bool isData = false;
  // MC16 Rel21 Recommendation
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibrationR21
  if(m_isMC16){
    jetCalibArea = "00-04-82";
    jetCalibConfig = "JES_MC16Recommendation_Consolidated_EMTopo_Apr2019_Rel21.config";
    jetCalibSeq = "JetArea_Residual_EtaJES_GSC_Smear";
  }
  // MC15 HLLHC mu200 ITK step 1.9
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibration2016
  if(m_isHLMC15){
    jetCalibArea = "00-04-82";
    jetCalibConfig = "JES_HLLHC_mu200_r9589_Aug2017.config";
    jetCalibSeq = "JetArea_Residual_Origin_EtaJES";
  }
  // Initialize JetCalibtool
  ANA_CHECK( m_jetCalibTool.setProperty("JetCollection",jetAlgo.Data()) );
  ANA_CHECK( m_jetCalibTool.setProperty("ConfigFile",jetCalibConfig.Data()) );
  ANA_CHECK( m_jetCalibTool.setProperty("CalibSequence",jetCalibSeq.Data()) );
  ANA_CHECK( m_jetCalibTool.setProperty("CalibArea",jetCalibArea.Data()) );
  ANA_CHECK( m_jetCalibTool.setProperty("IsData",isData) );
  ANA_CHECK( m_jetCalibTool.retrieve() );

  // Initialize the JetCalibration tool for AntiKt10LCTopoTrimmedPtFrac5SmallR20
  // TString largeRJetAlgo = "AntiKt10LCTopoTrimmedPtFrac5SmallR20";
  // TString largeRJetCalibArea, largeRJetCalibConfig, largeRJetCalibSeq;
  // MC16 Rel21 Recommendation
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibrationR21
  if(m_isMC16){

    TString largeRJetAlgo = "AntiKt10LCTopoTrimmedPtFrac5SmallR20";
    TString largeRJetCalibArea, largeRJetCalibConfig, largeRJetCalibSeq;

    largeRJetCalibArea = "00-04-82";
    largeRJetCalibConfig = "JES_MC16recommendation_FatJet_Trimmed_JMS_calo_12Oct2018.config";
    largeRJetCalibSeq = "EtaJES_JMS";

    ANA_CHECK( m_largeRJetCalibTool.setProperty("JetCollection",largeRJetAlgo.Data()) );
    ANA_CHECK( m_largeRJetCalibTool.setProperty("ConfigFile",largeRJetCalibConfig.Data()) );
    ANA_CHECK( m_largeRJetCalibTool.setProperty("CalibSequence",largeRJetCalibSeq.Data()) );
    ANA_CHECK( m_largeRJetCalibTool.setProperty("CalibArea",largeRJetCalibArea.Data()) );
    ANA_CHECK( m_largeRJetCalibTool.setProperty("IsData",isData) );
    ANA_CHECK( m_largeRJetCalibTool.retrieve() );
  }
  // MC15 HLLHC mu200 ITK step 1.9
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibration2016
  if(m_isHLMC15){
    ANA_MSG_INFO("No Large-R Jet Calibration has been officially derived for MC15 HL-LHC samples!");
  }
  // Initialize JetCalibtool
  // ANA_CHECK( m_largeRJetCalibTool.setProperty("JetCollection",largeRJetAlgo.Data()) );
  // ANA_CHECK( m_largeRJetCalibTool.setProperty("ConfigFile",largeRJetCalibConfig.Data()) );
  // ANA_CHECK( m_largeRJetCalibTool.setProperty("CalibSequence",largeRJetCalibSeq.Data()) );
  // ANA_CHECK( m_largeRJetCalibTool.setProperty("CalibArea",largeRJetCalibArea.Data()) );
  // ANA_CHECK( m_largeRJetCalibTool.setProperty("IsData",isData) );
  // ANA_CHECK( m_largeRJetCalibTool.retrieve() );

  // Initialize and configure the track selection tool
  m_trackSelTool = new InDet::InDetTrackSelectionTool("InDetTrackSelectionTool");
  ANA_CHECK (m_trackSelTool->setProperty( "minNSiHits", 9));
  // ANA_CHECK(m_trackSelTool->setProperty("maxZ0SinTheta",1.5));
  // ANA_CHECK(m_trackSelTool->setProperty("maxD0overSigmaD0",3.0));
  ANA_CHECK (m_trackSelTool->initialize());

  // Initialize and configure the photon ID tools
  m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector( "PhotonLooseIsEMSelector" );
  m_photonLooseIsEMSelector->setProperty("isEMMask",egammaPID::PhotonLoose);
  m_photonTightIsEMSelector = new AsgPhotonIsEMSelector( "PhotonTightIsEMSelector" );
  m_photonTightIsEMSelector->setProperty("isEMMask",egammaPID::PhotonTight);
  if (m_isMC16) {
    // set the file that contains the cuts on the shower shapes (stored in http://atlas.web.cern.ch/Atlas/GROUPS/DATABASE/GroupData/)
    ANA_CHECK( m_photonLooseIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc15_20150712/PhotonIsEMLooseSelectorCutDefs.conf") ); // Rel 21, "Loose" ID cut has not been updated in a while (not recommended for analysis; "Tight" is recommended for standard analyses")
    ANA_CHECK( m_photonTightIsEMSelector->setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/20180825/PhotonIsEMTightSelectorCutDefs.conf") ); // Rel 21, newest recommendation (though not documented) as of May, 12th, 2019

    ANA_CHECK( m_photonLooseIsEMSelector->initialize() ); 
    ANA_CHECK( m_photonTightIsEMSelector->initialize() );
  }
  // TODO: add photon ID definitions for other MC versions



  return StatusCode::SUCCESS;
}



StatusCode NtupleMakerAna :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  // retrieve the eventInfo object from the event store
  // and the event/run numbers from the eventInfo object
  const xAOD::EventInfo *eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  
  bool isMC = false;
  if (eventInfo->eventType (xAOD::EventInfo::IS_SIMULATION)) {
    isMC = true;
  }

  m_runNumber = eventInfo->runNumber();
  m_eventNumber = eventInfo->eventNumber();
  m_MU = eventInfo->averageInteractionsPerCrossing();
  if(isMC){
    // data has weight 1
    m_eventWeight = eventInfo->mcEventWeight(0)*m_overallWeight; //*lumi to get # of events
  }
  // print out run and event number from retrieved object
  if(m_verbose) ANA_MSG_INFO ( "EXECUTE: runNumber = " << m_runNumber << ", eventNumber = " << m_eventNumber );

  // retrieve the event shape object from the event store
  // and get the Event Sigma, Rho, and Area from that
  if (m_doEventShape) {
    const xAOD::EventShape *eshape = nullptr;
    ANA_CHECK(evtStore()->retrieve(eshape,"Kt4EMTopoEventShape"));
    double esrho = 0;
    double essigma = 0;
    double esarea = 0;
    eshape->getDensity(xAOD::EventShape::Density,esrho);
    eshape->getDensity(xAOD::EventShape::DensitySigma,essigma);
    eshape->getDensity(xAOD::EventShape::DensityArea,esarea);
    if (esrho < 1e-4) esrho = 1e-4;
    m_eventRho =   esrho/double(1000);
    m_eventSigma = essigma/double(1000);
    m_eventArea =  esarea;
  }

  // Clear all the vectors for every event
  if(m_doJets && m_doTracks){
    m_jetTrkPt->clear();
    m_jetTrkEta->clear();
    m_jetTrkPhi->clear();
    m_jetTrkZ0->clear();
    m_jetTrkD0->clear();
    m_jetTrkZ0err->clear();
    m_jetTrkD0err->clear();
    // m_jetTrkW->clear();
    m_jetTrkVertex->clear();
    m_jetTrkTruthVertex->clear();
    // m_jetTruthFraction->clear();
  }
  if(m_doVertices && m_doTracks){
    m_vtxTrkPt->clear();
    m_vtxTrkEta->clear();
    m_vtxTrkPhi->clear();
    m_vtxTrkZ0->clear();
    m_vtxTrkD0->clear();
    m_vtxTrkZ0err->clear();
    m_vtxTrkD0err->clear();
    m_vtxTrkW->clear();
    m_vtxTrkVertex->clear();
    m_vtxTrkTruthVertex->clear();
  }
  if(m_doJets){
    m_jetE_constit->clear();
    m_jetPt_constit->clear();
    m_jetEta_constit->clear();
    m_jetPhi_constit->clear();
    m_jetM_constit->clear();
    m_jetE_calib->clear();
    m_jetPt_calib->clear();
    m_jetEta_calib->clear();
    m_jetPhi_calib->clear();
    m_jetM_calib->clear();
    m_jetTiming->clear();
    m_jetNegE->clear();
    m_jetCharge->clear();
    m_jetAngularity->clear();
    m_jetFlavor->clear();
  }
  if(m_doLargeRJets){
    m_largeRjetE_constit->clear();
    m_largeRjetPt_constit->clear();
    m_largeRjetEta_constit->clear();
    m_largeRjetPhi_constit->clear();
    m_largeRjetM_constit->clear();
    m_largeRjetE_calib->clear();
    m_largeRjetPt_calib->clear();
    m_largeRjetEta_calib->clear();
    m_largeRjetPhi_calib->clear();
    m_largeRjetM_calib->clear();
    m_largeRjetFlavor->clear();
  }
  if(m_doJets && m_doTruthJets){
    m_jetType->clear();
    m_jetTruthPt->clear();
    m_jetTruthEta->clear();
    m_jetTruthPhi->clear();
  }
  if(m_doVertices){
    m_recovtx_z->clear();
    m_recovtx_z_err->clear();
    m_recovtx_sumPt->clear();
    m_recovtx_WsumPt->clear();
    m_recovtx_index->clear();
    if(m_doTruthVertices){
      m_truthvtx_index->clear();
      m_truthvtx_z->clear();
      m_truthvtx_x->clear();
      m_truthvtx_y->clear();
      m_truthvtx_t->clear();
    }
  }
  if(m_doPhotons){
    m_ptnE->clear();
    m_ptnPt->clear();
    m_ptnEta->clear();
    m_ptnPhi->clear();
    m_ptnIso_topoetcone20->clear();
    m_ptnIso_ptcone20->clear();
    // m_ptnVtx_z->clear();
    m_ptnIsLoose->clear();
    m_ptnIsTight->clear();
    if(m_doTruthPhotons){
      m_truthPtnE->clear();
      m_truthPtnPt->clear();
      m_truthPtnEta->clear();
      m_truthPtnPhi->clear();
    }
  }
  if(m_doTopoClusters){
    m_clPt->clear();
    m_clEta->clear();
    m_clPhi->clear();
    m_clLambda->clear();
    m_clLambda2->clear();
    m_clEmProb->clear();
  }

  // Truth Event Containers
  const xAOD::TruthEventContainer *tec = nullptr;
  ANA_CHECK(evtStore()->retrieve(tec,"TruthEvents"));
  const xAOD::TruthPileupEventContainer* tpec = nullptr;
  if(m_hasTruthPileupContainer){
    ANA_CHECK(evtStore()->retrieve(tpec, "TruthPileupEvents") ) ;
  }

  // Begin VERTEX Stuff
  if(m_doVertices){
    m_NPV = 0;
    const xAOD::VertexContainer *vxCont = nullptr;
    const xAOD::Vertex* pv = nullptr;
    ANA_CHECK(evtStore()->retrieve(vxCont, "PrimaryVertices") ) ;
    if(!vxCont->empty()) {
      for(const auto& vx : *vxCont){
        if(vx->vertexType()==xAOD::VxType::PriVtx)
          {pv = vx;}
        if(vx->vertexType()==xAOD::VxType::PriVtx || vx->vertexType()==xAOD::VxType::PileUp) {
          m_NPV++;
          m_recovtx_z->push_back(vx->z());
          m_recovtx_z_err->push_back(sqrt((vx->covariance())[vx->covariance().size() - 1]));
          m_recovtx_index->push_back(vx->index());
          m_vtxTrkPt->push_back(std::vector<float>());
          m_vtxTrkEta->push_back(std::vector<float>());
          m_vtxTrkPhi->push_back(std::vector<float>());
          m_vtxTrkZ0->push_back(std::vector<float>());
          m_vtxTrkD0->push_back(std::vector<float>());
          m_vtxTrkD0err->push_back(std::vector<float>());
          m_vtxTrkZ0err->push_back(std::vector<float>());
          m_vtxTrkW->push_back(std::vector<float>());
          m_vtxTrkVertex->push_back(std::vector<int>());
          m_vtxTrkTruthVertex->push_back(std::vector<int>());
          double sumPt = 0.0;
          double WsumPt = 0.0;
          int numTracks = vx->nTrackParticles();
          for(int id=0; id<numTracks;id++){            
            const xAOD::TrackParticle *trk = vx->trackParticle(id);
            if (!m_trackSelTool->accept(trk)) continue;
            if (!trk->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink").isValid()) continue;   
              const xAOD::TruthParticle *part0 = (*trk->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink"));

            int eventor = 0;
            int truthvert = -1;
            for(const auto& tpe : *tec) {
              for (int i = 0; i < tpe->nTruthParticles(); i++) {
                const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
                if (part==part0) {
                  truthvert = eventor;
                  break;
                }
              }//end for tpe->nTruthParticles()
            }//end for auto& tpe
            if(m_hasTruthPileupContainer){
              for(const auto& tpe : *tpec) {
                eventor++;
                for (int i = 0; i < tpe->nTruthParticles(); i++) {
                  const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
                  if (part==part0) {
                    truthvert = eventor;
                    break;
                  }
                }// end for tpe->nTruthParticles()
              }//end for auto& tpe
            }
            sumPt+=(trk->pt()/1000.0)*(trk->pt()/1000.0);
            WsumPt+=(trk->vertex()->trackWeight(id))*(trk->pt()/1000.0)*(trk->pt()/1000.0);
            m_vtxTrkPt->at(m_vtxTrkPt->size()-1).push_back(trk->pt()/1000.0);//Track pt 1 float per loop
            m_vtxTrkEta->at(m_vtxTrkEta->size()-1).push_back(trk->eta());//Track eta 1 float per loop
            m_vtxTrkPhi->at(m_vtxTrkPhi->size()-1).push_back(trk->phi());//Track phi 1 float per loop
            m_vtxTrkTruthVertex->at(m_vtxTrkTruthVertex->size()-1).push_back(truthvert);// Truth vertex of track 1 int per loop
            m_vtxTrkVertex->at(m_vtxTrkVertex->size()-1).push_back(trk->vertex()?trk->vertex()->index():-1); //Vertex intdex of track 1 int per loop
            m_vtxTrkZ0->at(m_vtxTrkZ0->size()-1).push_back(trk->z0());//Track z0 1 float per loop
            m_vtxTrkD0->at(m_vtxTrkD0->size()-1).push_back(trk->d0());//Track d0 1 float per loop
            m_vtxTrkD0err->at(m_vtxTrkD0err->size()-1).push_back(sqrt(trk->definingParametersCovMatrix()(0,0)));
            m_vtxTrkZ0err->at(m_vtxTrkZ0err->size()-1).push_back(sqrt(trk->definingParametersCovMatrix()(1,1)));
            m_vtxTrkW->at(m_vtxTrkW->size()-1).push_back(trk->vertex()->trackWeight(id));
          }//end for tracks
          sumPt=sqrt(sumPt);
          WsumPt=sqrt(WsumPt);
          m_recovtx_sumPt->push_back(sumPt);
          m_recovtx_WsumPt->push_back(WsumPt);
        }//end if pvx or pile up 
      }//end for vtxs
      //-----------------------------------------------------------------------------
      if (!pv) return StatusCode::SUCCESS;
    }//end not empty

    if(m_doTruthVertices){
      // PVind = pv->index();
      const xAOD::TruthVertexContainer *tvxCont = 0;
      ANA_CHECK(evtStore()->retrieve(tvxCont, "TruthVertices"));
      bool mark = false;
      if (fabs(pv->z()-(*tec->begin())->signalProcessVertex()->z())<0.1) mark = true;
      // if (mark) VERT = 1; 
      m_truthvtx_z->push_back((*tec->begin())->signalProcessVertex()->z());
      m_truthvtx_y->push_back((*tec->begin())->signalProcessVertex()->y());
      m_truthvtx_x->push_back((*tec->begin())->signalProcessVertex()->x());
      m_truthvtx_t->push_back((*tec->begin())->signalProcessVertex()->t()/0.299792458);
      m_truthvtx_index->push_back(0);

      //Truth Pileup Event Loop
      //-----
      int eventor = 0;
      if(m_hasTruthPileupContainer){
        for(const auto& tpe : *tpec) {
          eventor++;
          double tpez = 0;
          double tpey = 0;
          double tpex = 0;
          double tpet = 0;
          double tidx = 0;
          int barcodeMin = 999;
          for (size_t i = 0; i < tpe->nTruthVertices(); i++) {
            if( abs(tpe->truthVertex(i)->barcode()) < barcodeMin ){
              barcodeMin = abs(tpe->truthVertex(i)->barcode());
              tpez = tpe->truthVertex(i)->z();
              tpey = tpe->truthVertex(i)->y();
              tpex = tpe->truthVertex(i)->x();
              tpet = tpe->truthVertex(i)->t();
            }
          }//end for tpe->nTruthVertices()
          m_truthvtx_z->push_back(tpez);
          m_truthvtx_y->push_back(tpey);
          m_truthvtx_x->push_back(tpex);
          m_truthvtx_t->push_back(tpet/0.299792458);
          m_truthvtx_index->push_back(eventor);  
        }//----- END truth pileup event container loop
      }
    }
  }
  // End VERTEX Stuff

  // BEGIN jet stuff
  if(m_doJets){
    // get jet container of interest
    const xAOD::JetContainer* jets = 0;
    ANA_CHECK(evtStore()->retrieve( jets, "AntiKt4EMTopoJets"));
    // jet cleaning
    unsigned int numGoodJets = 0;
    if(m_verbose) ANA_MSG_INFO ("EXECUTE: number of reco. jets = " << jets->size());

    // Placeholder variables to retrieve jet variables
    float j_Timing = 0;
    float j_NegE = 0;
    float j_Charge = 0;
    float j_Angularity = 0;
    int j_Flavor = 0;

    for (const xAOD::Jet* jet : *jets) {
      if (!m_jetCleaning->keep (*jet)) continue; // only keep good clean jets
      ++numGoodJets;

      if (isMC) {
        // get the jet energy MC resolution
        // double mcRes = m_JERTool->getRelResolutionMC(jet);
        // get the resolution uncertainty
        // double mcRes_uncert = m_JERTool->getUncertainty(jet);
        // For now, we don't do anything with these MC smearing...
        jet->getAttribute<int>("PartonTruthLabelID", j_Flavor);
        m_jetFlavor->push_back (j_Flavor); 
      }

      xAOD::Jet * cjet = 0;
      m_jetCalibTool->calibratedCopy(*jet, cjet); // calibrated copy
      
      // Jet cut: calibrated pT > 15 GeV
      if (cjet->pt() / 1000. < 15) {
        delete cjet;
        continue;
      }

      // retrieve the most basic variables
      // fill in constituent scale, uncalibrated kinematics
      m_jetE_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).E());
      m_jetPt_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Pt());
      m_jetEta_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Eta());
      m_jetPhi_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Phi());
      m_jetM_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).M());
      // fill in calibrated kinematics
      m_jetE_calib->push_back (cjet->e()/1000.);
      m_jetPt_calib->push_back (cjet->pt()/1000.);
      m_jetEta_calib->push_back (cjet->eta());
      m_jetPhi_calib->push_back (cjet->phi());
      m_jetM_calib->push_back (cjet->m()/1000.);
      // fill in non-kinematic variables
      cjet->getAttribute("Timing", j_Timing);
      m_jetTiming->push_back (j_Timing);
      cjet->getAttribute("NegativeE", j_NegE);
      m_jetNegE->push_back (j_NegE);
      cjet->getAttribute("Charge", j_Charge);
      m_jetCharge->push_back (j_Charge);
      cjet->getAttribute("Angularity", j_Angularity);
      m_jetAngularity->push_back (j_Angularity);

      if(m_doTracks){
        m_jetTrkPt->push_back(std::vector<float>());
        m_jetTrkEta->push_back(std::vector<float>());
        m_jetTrkPhi->push_back(std::vector<float>());
        m_jetTrkZ0->push_back(std::vector<float>());
        m_jetTrkD0->push_back(std::vector<float>());
        m_jetTrkD0err->push_back(std::vector<float>());
        m_jetTrkZ0err->push_back(std::vector<float>());
        // m_jetTrkW->push_back(std::vector<float>());
        m_jetTrkVertex->push_back(std::vector<int>());
        m_jetTrkTruthVertex->push_back(std::vector<int>());
        std::vector<const xAOD::IParticle*> jettracks;
        cjet->getAssociatedObjects<xAOD::IParticle>(xAOD::JetAttribute::GhostTrack,jettracks);
        for(const auto& ptrk : jettracks) {
          const xAOD::TrackParticle *trk = static_cast<const xAOD::TrackParticle*>(ptrk);
          if (!m_trackSelTool->accept(trk)) continue;
          if (!trk->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink").isValid()) continue;
          const xAOD::TruthParticle *part0 = (*trk->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink"));
          int eventor = 0;
          int truthvert = -1;
          for(const auto& tpe : *tec) {
            for (int i = 0; i < tpe->nTruthParticles(); i++) {
              const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
              if (part==part0) {
                truthvert = eventor;
                break;
              }
            }//end for tpe->nTruthParticles()
          }//end for auto& tpe
          if(m_hasTruthPileupContainer){
            for(const auto& tpe : *tpec) {
              eventor++;
              for (int i = 0; i < tpe->nTruthParticles(); i++) {
                const xAOD::TruthParticle *part = static_cast<const xAOD::TruthParticle*>(tpe->truthParticle(i));
                if (part==part0) {
                  truthvert = eventor;
                  break;
                }
              }// end for tpe->nTruthParticles()
            }//end for auto& tpe
          }
          m_jetTrkPt->at(m_jetTrkPt->size()-1).push_back(trk->pt()/1000.0);//Track pt 1 float per loop
          m_jetTrkEta->at(m_jetTrkEta->size()-1).push_back(trk->eta());//Track eta 1 float per loop
          m_jetTrkPhi->at(m_jetTrkPhi->size()-1).push_back(trk->phi());//Track phi 1 float per loop
          m_jetTrkTruthVertex->at(m_jetTrkTruthVertex->size()-1).push_back(truthvert);// Truth vertex of track 1 int per loop
          m_jetTrkVertex->at(m_jetTrkVertex->size()-1).push_back(trk->vertex()?trk->vertex()->index():-1); //Vertex intdex of track 1 int per loop
          m_jetTrkZ0->at(m_jetTrkZ0->size()-1).push_back(trk->z0());//Track z0 1 float per loop
          m_jetTrkD0->at(m_jetTrkD0->size()-1).push_back(trk->d0());//Track d0 1 float per loop
          m_jetTrkD0err->at(m_jetTrkD0err->size()-1).push_back(trk->definingParametersCovMatrix()(0,0));
          m_jetTrkZ0err->at(m_jetTrkZ0err->size()-1).push_back(trk->definingParametersCovMatrix()(1,1));
          // m_jetTrkW[m_jetTrkW.size()-1].push_back(trk->vertex()->trackWeight(trk->index()));
        }//END for jettracks
      }
      if(m_doTruthJets){
        const xAOD::JetContainer* jetContTruth(0);
        ANA_CHECK(evtStore()->retrieve(jetContTruth, "AntiKt4TruthJets"));
        bool hs = false;
        bool pu = true;
        double largestHsJetPt = 0;
        double largestHsJetEta =0;
        double largestHsJetPhi =0;
        for (const auto& jettr : *jetContTruth) {
          if (jettr->pt()>4000. && jettr->p4().DeltaR(cjet->p4())<0.6) pu = false;
          if (jettr->pt()>10000. && jettr->p4().DeltaR(cjet->p4())<0.3) hs = true;
          if (jettr->pt()>10000. && jettr->p4().DeltaR(cjet->p4())<0.3 && jettr->pt()>largestHsJetPt){ 
            largestHsJetPt = jettr->pt();
            largestHsJetEta = jettr->eta();
            largestHsJetPhi = jettr->phi();
          }
        }//end auto& jettr jetContTruth
        m_jetTruthPt->push_back(hs?largestHsJetPt/1000.:-1);
        m_jetTruthEta->push_back(hs?largestHsJetEta:-999);
        m_jetTruthPhi->push_back(hs?largestHsJetPhi:-999);
        m_jetType->push_back(hs?1:(pu?2:0));
      }


      // delete the newly created, calibrated copy jet
      // could lead to memory leak!
      delete cjet;
    }
    if(m_verbose) ANA_MSG_INFO ("EXECUTE: number of good clean reco. jets = " << numGoodJets);
  }
  // END jet stuff

  // BEGIN Large R jet stuff
  if (m_doLargeRJets){
    // get jet container of interest
    const xAOD::JetContainer* largeRjets = 0;
    ANA_CHECK (evtStore()->retrieve( largeRjets, "AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets"));

    // Placeholder variables to retrieve jet variables
    int j_Flavor = 0;

    for (const xAOD::Jet* jet : *largeRjets) {
      if (isMC) {
        //Truth info
        jet->getAttribute<int>("PartonTruthLabelID", j_Flavor);
        m_largeRjetFlavor->push_back (j_Flavor); 
      }

      xAOD::Jet * cjet = 0;
      m_largeRJetCalibTool->calibratedCopy(*jet, cjet); // calibrated copy
      
      // Large-R Jet cut: calibrated pT > 100 GeV
      if (cjet->pt() / 1000. < 100) {
        delete cjet;
        continue;
      }

      // retrieve the most basic variables
      // fill in constituent scale, uncalibrated kinematics
      m_largeRjetE_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).E());
      m_largeRjetPt_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Pt());
      m_largeRjetEta_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Eta());
      m_largeRjetPhi_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).Phi());
      m_largeRjetM_constit->push_back (jet->jetP4(xAOD::JetConstitScaleMomentum).M());
      // fill in calibrated kinematics
      m_largeRjetE_calib->push_back (cjet->e()/1000.);
      m_largeRjetPt_calib->push_back (cjet->pt()/1000.);
      m_largeRjetEta_calib->push_back (cjet->eta());
      m_largeRjetPhi_calib->push_back (cjet->phi());
      m_largeRjetM_calib->push_back (cjet->m()/1000.);

      delete cjet;
    }

  }
  // END large R jet stuff
  
  // BEGIN photon stuff
  if (m_doPhotons) {
    // get (reco.) photon container
    const xAOD::PhotonContainer* ptns = 0;
    ANA_CHECK (evtStore()->retrieve( ptns, "Photons" ));

    // Placeholder variables to retrieve photon variables
    // float p_vtx_z;

    for (const xAOD::Photon* ptn : *ptns) {
      // retrieve 4-vector and basic photon attributes
      m_ptnE->push_back (ptn->e());
      m_ptnPt->push_back (ptn->pt());
      m_ptnEta->push_back (ptn->eta());
      m_ptnPhi->push_back (ptn->phi());
      m_ptnIso_topoetcone20->push_back (ptn->isolation(xAOD::Iso::topoetcone20));
      m_ptnIso_ptcone20->push_back (ptn->isolation(xAOD::Iso::ptcone20));
      // ptn->getAttribute("topoetcone20", p_topoetcone20);
      // m_ptnIso_topoetcone20->push_back (p_topoetcone20);
      // ptn->getAttribute("ptcone20", p_ptcone20);
      // m_ptnIso_ptcone20->push_back (p_ptcone20);
      m_ptnIsLoose->push_back (m_photonLooseIsEMSelector->accept(ptn));
      m_ptnIsTight->push_back (m_photonTightIsEMSelector->accept(ptn));
    }
  }
  // END photon stuff
  

  // BEGIN truth photon stuff
  if (m_doTruthPhotons) {
    // get truth e/gamma particles container
    const xAOD::TruthParticleContainer* egammas = 0;
    ANA_CHECK (evtStore()->retrieve( egammas, "egammaTruthParticles" ));

    for (const xAOD::TruthParticle* egamma : *egammas) {
      if (egamma->absPdgId() == 22) {
        m_truthPtnE->push_back (egamma->e());
        m_truthPtnPt->push_back (egamma->pt());
        m_truthPtnEta->push_back (egamma->eta());
        m_truthPtnPhi->push_back (egamma->phi());
      }
    }
  }

  // Fill the event into the tree:
  tree ("ntuple")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode NtupleMakerAna :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}
