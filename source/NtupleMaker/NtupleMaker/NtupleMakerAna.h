#ifndef NtupleMaker_NtupleMakerAna_H
#define NtupleMaker_NtupleMakerAna_H

// C++ stuff
#include <vector>

// Root stuff
#include <TTree.h>

// Basic analysis set-ups
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>

// Jet tools
#include <JetInterface/IJetSelector.h>
#include <JetResolution/IJERTool.h>
#include <JetCalibTools/IJetCalibrationTool.h>

// Track tools
#include "InDetTrackSelectionTool/InDetTrackSelectionTool.h"

// Photon tools
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"

/// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"  

/// For xAOD includes:
#include "xAODTruth/TruthParticle.h"
#include "xAODTracking/TrackParticle.h"
#include "AthLinks/ElementLink.h"
#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"

class NtupleMakerAna : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor and destructor
  NtupleMakerAna (const std::string& name, ISvcLocator* pSvcLocator);//!
  ~NtupleMakerAna () override;//!

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;//!
  virtual StatusCode execute () override;//!
  virtual StatusCode finalize () override;//!

private:
  // Configuration, and any other types of variables go here.

  // Flags for the analysis:
  bool m_verbose;//!
  bool m_doEventShape;//!
  bool m_doLargeRJets;//!
  bool m_doJets;//!
  bool m_doTruthJets;//!
  // bool m_jetConstits;//!
  bool m_doTruthLargeRJets;//!
  bool m_doVertices;//!
  bool m_doTruthVertices;//!
  bool m_doPhotons;//!
  bool m_doTruthPhotons;//!
  bool m_doTopoClusters;//!
  bool m_doTracks;//!
  bool m_isMC16;//!
  bool m_isHLMC15;//!
  bool m_hasTruthPileupContainer;//!
  float m_overallWeight;//!

  // Analysis and processing tools
  asg::AnaToolHandle<IJetSelector> m_jetCleaning;//!
  asg::AnaToolHandle<IJERTool> m_JERTool;//!
  asg::AnaToolHandle<IJetCalibrationTool> m_jetCalibTool;//!
  asg::AnaToolHandle<IJetCalibrationTool> m_largeRJetCalibTool;//!
  InDet::InDetTrackSelectionTool* m_trackSelTool;//!
  AsgPhotonIsEMSelector* m_photonLooseIsEMSelector;//!
  AsgPhotonIsEMSelector* m_photonTightIsEMSelector;//!


  // Variables used during ntuple creation
  // Event-level
  unsigned int m_runNumber = 0;//!              // Run number for the current event
  unsigned long long m_eventNumber = 0;//!      // Event number
  int m_MU = 0;//!
  float m_eventWeight = 1.;//!   
  float m_eventRho = 0;//!
  float m_eventSigma = 0;//!
  float m_eventArea = 0;//!
  int m_NPV = 0;//!

  // Small R Jet variables
  // `_constit` means constituent scale (i.e. no calibration)
  std::vector<float> *m_jetE_constit;//! 
  std::vector<float> *m_jetPt_constit;//!
  std::vector<float> *m_jetEta_constit;//!
  std::vector<float> *m_jetPhi_constit;//!
  std::vector<float> *m_jetM_constit;//!
  // `_calib` means calibrated according to available JetCalibrationTools
  std::vector<float> *m_jetE_calib;//! 
  std::vector<float> *m_jetPt_calib;//!
  std::vector<float> *m_jetEta_calib;//!
  std::vector<float> *m_jetPhi_calib;//!
  std::vector<float> *m_jetM_calib;//!
  // Small R Jet variables other than kinematics
  std::vector<float> *m_jetTiming;//!
  std::vector<float> *m_jetNegE;//!
  std::vector<float> *m_jetCharge;//!
  std::vector<float> *m_jetAngularity;//!
  std::vector<int> *m_jetFlavor;//!

  /// Small R Jet truth variables
  std::vector<int> *m_jetType;//!
  std::vector<float> *m_jetTruthPt;//!
  std::vector<float> *m_jetTruthEta;//!
  std::vector<float> *m_jetTruthPhi;//!
  //  std::vector<int> jetTruthId;//!

  /// Small R jet track variables
  std::vector<std::vector<float> > *m_jetTrkPt;//!
  std::vector<std::vector<float> > *m_jetTrkEta;//!
  std::vector<std::vector<float> > *m_jetTrkPhi;//!
  std::vector<std::vector<float> > *m_jetTrkZ0;//!
  std::vector<std::vector<float> > *m_jetTrkD0;//!
  std::vector<std::vector<float> > *m_jetTrkZ0err;//!
  std::vector<std::vector<float> > *m_jetTrkD0err;//!
  // std::vector<std::vector<float> > *m_jetTrkW;//!
  std::vector<std::vector<int> > *m_jetTrkVertex;//!
  std::vector<std::vector<int> > *m_jetTrkTruthVertex;//!
  // std::vector<std::vector<float> > *m_jetTruthFraction;//!

  /// Large R Jet variables
  // `_constit` means constituent scale (i.e. no calibration)
  std::vector<float> *m_largeRjetE_constit;//! 
  std::vector<float> *m_largeRjetPt_constit;//!
  std::vector<float> *m_largeRjetEta_constit;//!
  std::vector<float> *m_largeRjetPhi_constit;//!
  std::vector<float> *m_largeRjetM_constit;//!
  // `_calib` means calibrated according to available JetCalibrationTools
  std::vector<float> *m_largeRjetE_calib;//! 
  std::vector<float> *m_largeRjetPt_calib;//!
  std::vector<float> *m_largeRjetEta_calib;//!
  std::vector<float> *m_largeRjetPhi_calib;//!
  std::vector<float> *m_largeRjetM_calib;//!
  std::vector<int> *m_largeRjetFlavor;//! 

  /// vtx variables
  std::vector<float> *m_recovtx_z;//!
  std::vector<float> *m_recovtx_z_err;//!
  std::vector<float> *m_recovtx_sumPt;//!
  std::vector<float> *m_recovtx_WsumPt;//!
  std::vector<int> *m_recovtx_index;//!
  std::vector<int> *m_truthvtx_index;//!
  std::vector<float> *m_truthvtx_z;//!
  std::vector<float> *m_truthvtx_x;//!
  std::vector<float> *m_truthvtx_y;//!
  std::vector<float> *m_truthvtx_t;//!

  /// vtx track variables
  std::vector<std::vector<float> > *m_vtxTrkPt;//!
  std::vector<std::vector<float> > *m_vtxTrkEta;//!
  std::vector<std::vector<float> > *m_vtxTrkPhi;//!
  std::vector<std::vector<float> > *m_vtxTrkZ0;//!
  std::vector<std::vector<float> > *m_vtxTrkD0;//!
  std::vector<std::vector<float> > *m_vtxTrkZ0err;//!
  std::vector<std::vector<float> > *m_vtxTrkD0err;//!
  std::vector<std::vector<float> > *m_vtxTrkW;//!
  std::vector<std::vector<int> > *m_vtxTrkVertex;//!
  std::vector<std::vector<int> > *m_vtxTrkTruthVertex;//!

  /// photon variables
  std::vector<float> *m_ptnE;//!
  std::vector<float> *m_ptnPt;//!
  std::vector<float> *m_ptnEta;//!
  std::vector<float> *m_ptnPhi;//!
  std::vector<float> *m_ptnIso_topoetcone20;//!
  std::vector<float> *m_ptnIso_ptcone20;//!
  // std::vector<float> *m_ptnVtx_z;//!
  std::vector<int> *m_ptnIsLoose;//!
  std::vector<int> *m_ptnIsTight;//!
  std::vector<float> *m_truthPtnE;//!
  std::vector<float> *m_truthPtnPt;//!
  std::vector<float> *m_truthPtnEta;//!
  std::vector<float> *m_truthPtnPhi;//!

  /// topocluster variables
  std::vector<float> *m_clPt;//!
  std::vector<float> *m_clEta;//!
  std::vector<float> *m_clPhi;//!
  std::vector<float> *m_clLambda;//!
  std::vector<float> *m_clLambda2;//!
  std::vector<float> *m_clEmProb;//!

};//!

#endif
